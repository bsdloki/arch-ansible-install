- hosts: arch
  vars_prompt:
          - name: user_name
            prompt: "Enter username: "
            private: no
          - name: user_password
            prompt: "Enter password: "
            private: yes
  tasks:
          - name: Abort if it's not install media
            fail:
                    msg: "This host is not booted from archiso"
            when: ansible_nodename != 'archiso'

          - name: Set time and date
            command: timedatectl set-ntp true

          - name: Disk setup
            block:
                    - name: Partition disks
                      parted:
                              device: /dev/sda
                              label: msdos
                              part_type: primary
                              number: 1
                              part_start: 1MiB
                              part_end: 100%
                              state: present

                    - name: Format disk
                      filesystem:
                              fstype: btrfs
                              dev: /dev/sda1

                    - name: Mount disk
                      mount:
                              path: /mnt
                              src: /dev/sda1
                              fstype: btrfs
                              state: mounted

                    - name: Create boot folder
                      file:
                              path: /mnt/boot
                              state: directory

          - name: Install base system
            block: 
                - name: Install pacman-contrib
                  pacman:
                          name:
                                  - pacman-contrib
                          update_cache: yes
                - name: Rank mirrors
                  shell:  curl -s "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist

                        
                - name: Pacstrap system
                  command: pacstrap /mnt base base-devel linux linux-firmware btrfs-progs open-vm-tools neovim grub os-prober openssh sudo netctl

          - name: Generate fstab
            command: genfstab -U /mnt >> /mnt/etc/fstab

          - name: Set timezone
            command: arch-chroot /mnt ln -sf /usr/share/zoneinfo/US/Central /etc/localtime

          - name: Set hardware clock
            command: arch-chroot /mnt hwclock --systohc

          - name: Locales
            block:
                    - name: Configure locale.gen
                      lineinfile: 
                        path: /mnt/etc/locale.gen
                        line: en_US.UTF-8 UTF-8

                    - name: Create locale.conf
                      copy:
                              content: "LANG=en_US.UTF-8"
                              dest: /mnt/etc/locale.conf

                    - name: Generate locales
                      command: arch-chroot /mnt locale-gen

          - name: Set hostname
            copy:
                   content: '{{ inventory_hostname }}'
                   dest: /mnt/etc/hostname
          - name: Set hosts
            copy:
                   content: |
                           127.0.0.1       localhost
                           127.0.1.1       {{ inventory_hostname }}.loki.local    {{ inventory_hostname }}
                   dest: /mnt/etc/hosts

          - name: Netctl
            block:
                   - name: Get current ipv4 address
                     shell: ip a | grep ens192 | grep inet | awk '{print $2}' | cut -d '/' -f 1
                     register: current_ipv4_address

                   - name: Static addressing ipv4
                     copy:
                             content: |
                                     Description='Static ethernet'
                                     Interface=ens192
                                     Connection=ethernet
                                     IP=static
                                     Address=('{{ current_ipv4_address.stdout }}/24')
                                     Gateway='10.0.1.1'
                                     DNS=('10.0.1.254')
                             dest: /mnt/etc/netctl/ens192
          - name: Enable sshd
            command: arch-chroot /mnt systemctl enable sshd

          - name: Enable netctl
            command: arch-chroot /mnt netctl enable ens192

          - name: Enable vmtools
            block:
                    - name: vmtoolsd
                      command: arch-chroot /mnt systemctl enable vmtoolsd.service
                    - name: vmware-vmblock-fuse
                      command: arch-chroot /mnt systemctl enable vmware-vmblock-fuse.service
         
          - name: GRUB
            block:
                   - name: Install GRUB
                     command: arch-chroot /mnt grub-install /dev/sda

                   - name: Make grub config
                     command: arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

          - name: Create account
            command: arch-chroot /mnt useradd -U -G wheel -k /etc/skel -s /bin/bash -m --password {{ user_password | password_hash('sha512') }} {{ user_name }}

          - name: Add wheel to sudo
            copy:
                content: '%wheel ALL=(ALL) NOPASSWD: ALL'
                dest: /mnt/etc/sudoers.d/wheel
                validate: /usr/sbin/visudo --check --file=%s

          - name: Add ssh keys
            block:
                  - name: Create ssh directory
                    file:
                            path: /mnt/home/{{ user_name }}/.ssh
                            state: directory
                  - name: Set perms
                    command: arch-chroot /mnt {{ item }}
                    loop:
                            - chmod 0700 /home/{{ user_name }}/.ssh
                            - chown {{ user_name }}:{{ user_name }} /home/{{ user_name }}/.ssh

                  - name: Copy SSH key
                    copy:
                            src: /home/loki/.ssh/id_rsa.pub
                            dest: /mnt/home/{{ user_name }}/.ssh/authorized_keys

                  - name: Set key file perms
                    command: arch-chroot /mnt {{ item }}
                    loop:
                            - chmod 0600 /home/{{ user_name }}/.ssh/authorized_keys
                            - chown {{ user_name }}:{{ user_name }} /home/{{ user_name }}/.ssh/authorized_keys

          - name: reboot
            reboot:
                   connect_timeout: 1
                   reboot_timeout: 1
            failed_when: false
